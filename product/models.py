from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=200)
    product_type = models.CharField(max_length=200)
    description = models.TextField()
    stock = models.IntegerField()
    price = models.IntegerField()
    origin = models.CharField(max_length=200)
    image_source = models.CharField(max_length=200)

    def __str__(self):
        return self.product_name
