# Generated by Django 2.1.5 on 2020-08-30 18:03

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_name', models.TextField(max_length=100)),
                ('product_type', models.TextField(max_length=100)),
                ('description', models.TextField(max_length=100)),
                ('stock', models.IntegerField()),
                ('price', models.IntegerField()),
                ('origin', models.TextField()),
                ('image_source', models.TextField()),
            ],
        ),
    ]
