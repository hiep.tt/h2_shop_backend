from rest_framework import generics
from rest_framework.authentication import TokenAuthentication

from .models import Product
from .serializers import ProductSerializer
from .permissions import IsAuthorOrReadOnly


class ProductList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
